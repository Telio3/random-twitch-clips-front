import { HttpParams } from '@angular/common/http';

export const environment = {
  production: true,
  url_api: '/api',
  httpOptions: {
    params: new HttpParams(),
  },
};
