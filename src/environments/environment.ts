import { HttpParams } from '@angular/common/http';

export const environment = {
  production: false,
  url_api: '/api',
  httpOptions: {
    params: new HttpParams(),
  },
};
