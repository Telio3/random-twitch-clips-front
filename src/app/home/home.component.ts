import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  private isLoggedin$: Observable<boolean> =
    this.authService.isLoggedin$.asObservable();

  public isLoggedin: boolean = false;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.isLoggedin$.subscribe((isLoggedin) => (this.isLoggedin = isLoggedin));
  }
}
