export interface User {
  _id?: string;
  twitchId?: string;
  name?: string;
  broadcasterType?: string;
  description?: string;
  profileImageUrl?: string;
  offlineImageUrl?: string;
  createdAt?: string;
}
