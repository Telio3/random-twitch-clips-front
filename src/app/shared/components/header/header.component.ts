import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../../interfaces/user.interface';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isLoggedin$: Observable<boolean> =
    this.authService.isLoggedin$.asObservable();

  private user$: Observable<User | null> =
    this.authService.user$.asObservable();

  public userObject: User | null = null;

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit(): void {
    this.user$.subscribe((user) => (this.userObject = user));
  }

  goToProfile() {
    this.isLoggedin$.subscribe((isLoggedin) => {
      if (isLoggedin) {
        this.router.navigate(['/profile']);
      } else {
        window.location.href = 'https://127.0.0.1:443/auth/twitch';
      }
    });
  }
}
