import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public isLoggedin$: ReplaySubject<boolean> = new ReplaySubject(1);
  public user$: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(
    null
  );

  constructor(private http: HttpClient) {}

  fetchCurrentUser(): Observable<User> {
    return this.http.get<User>(environment.url_api + '/user/details').pipe(
      tap((user: User) => {
        this.user$.next(user);
        if (user) {
          this.isLoggedin$.next(true);
        } else {
          this.isLoggedin$.next(false);
        }
      })
    );
  }

  logout(): Observable<any> {
    return this.http.get(environment.url_api + '/auth/signout').pipe(
      tap(() => {
        this.user$.next(null);
      })
    );
  }
}
